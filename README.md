# LoRaWAN Car Parking Node
Every driver wastes on average about 100 hours a year looking for a parking space, which accounts for one third of city center traffic, so that it is very important to “know where to park without searching and how long the vehicle parked inside the slot” to get know how much the owner has to pay. ICFOSS developed a magnetometer based low power, LoRaWAN integrated car parking sensor-controller embedded system that helps to optimize the use of urban parking facilities to substantially reduce the congestion caused by motorists searching for a space. Which can also be used where parking solutions are necessary like Malls, Industrial areas etc. LoRaWAN, Low cost and Intelligent power consumption based on the vehicle movement features are going to cut down cost of mainitaning parking solutions exponentially.




![Smart car parking Node](https://gitlab.com/manumsm2/car_parking_node/-/raw/main/IMG_20230921_145733.jpg?ref_type=heads)




### Prerequisites
1. Arduino IDE 
2. ULPLoRa  (https://gitlab.com/icfoss/OpenIoT/ulplora)
3. QMC5883L (Magnetometer)

## contributing
Instructions coming soon

### License
This project is licensed under the MIT License - see the LICENSE.md file for details
## Acknowledgments
1. Sooraj V S
2. Manu Mohan M S
3. Aiswarya Babu
4. Rosemina Joseph
5. Jaison Jacob
6. Gopika T G

